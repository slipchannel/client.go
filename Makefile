.DEFAULT_GOAL := $(LOCAL_BINARY)
.PHONY: clean dist

BINARY = slipchannel

SOURCEDIR ?= .
SOURCES := $(shell find $(SOURCEDIR) -name "*.go")

PKG_NAME := gitlab.com/slipchannel/client.go/cli/slipchannel
DIST_DIR ?= $(GOPATH)/dist
BIN_DIR ?= $(GOPATH)/bin

LOCAL_BINARY := $(BIN_DIR)/$(BINARY)

LDFLAGS := -ldflags "-X main.Build=$(shell git rev-parse --short HEAD)"

PREPARE_COMMAND = mkdir -p $(DIST_DIR)/$(OS)-$(ARCH)
BUILD_COMMAND   = GOOS=$(OS) GOARCH=$(ARCH) godep go build $(LDFLAGS) -o $(DIST_DIR)/$(OS)-$(ARCH)/slipchannel $(PKG_NAME)
PKG_COMMAND     = tar cfz $(DIST_DIR)/slipchannel.$(VERSION).$(OS)-$(ARCH).tar.gz -C $(DIST_DIR)/$(OS)-$(ARCH) .

define package =
	$(eval OS_ARCH := $(1)-$(2))
	$(eval ARCH_DIST_DIR := $(DIST_DIR)/$(OS_ARCH))
	mkdir -p $(ARCH_DIST_DIR)
	GOOS=$(1) GOARCH=$(2) go build -o $(ARCH_DIST_DIR)/slipchannel $(PKG_NAME)
	tar cfz $(DIST_DIR)/slipchannel.$(3).$(OS_ARCH).tar.gz -C $(ARCH_DIST_DIR) .
endef

$(LOCAL_BINARY): $(SOURCES)
	godep go install $(LDFLAGS) $(PKG_NAME)

clean:
	rm -rf $(DIST_DIR)

$(DIST_DIR):
	mkdir -p $(DIST_DIR)

dist: $(DIST_DIR)
ifndef VERSION
	$(error VERSION is required to continue)
endif

	$(eval OS := darwin)
	$(eval ARCH := amd64)
	$(PREPARE_COMMAND)
	$(BUILD_COMMAND)
	$(PKG_COMMAND)

	$(eval OS := linux)
	$(eval ARCH := 386)
	$(PREPARE_COMMAND)
	$(BUILD_COMMAND)
	$(PKG_COMMAND)

	$(eval OS := linux)
	$(eval ARCH := arm)
	$(PREPARE_COMMAND)
	$(BUILD_COMMAND)
	$(PKG_COMMAND)

	$(eval OS := linux)
	$(eval ARCH := amd64)
	$(PREPARE_COMMAND)
	$(BUILD_COMMAND)
	$(PKG_COMMAND)

	$(eval OS := linux)
	$(eval ARCH := arm64)
	$(PREPARE_COMMAND)
	$(BUILD_COMMAND)
	$(PKG_COMMAND)

	$(eval OS := freebsd)
	$(eval ARCH := amd64)
	$(PREPARE_COMMAND)
	$(BUILD_COMMAND)
	$(PKG_COMMAND)

	$(eval OS := openbsd)
	$(eval ARCH := amd64)
	$(PREPARE_COMMAND)
	$(BUILD_COMMAND)
	$(PKG_COMMAND)

	$(eval OS := netbsd)
	$(eval ARCH := amd64)
	$(PREPARE_COMMAND)
	$(BUILD_COMMAND)
	$(PKG_COMMAND)

broken:
	#$(call package,darwin,amd64,$(VERSION))
	#$(call package,linux,386,$(VERSION))
	#$(call package,linux,arm,$(VERSION))
	#$(call package,linux,amd64,$(VERSION))
	#$(call package,linux,arm64,$(VERSION))
	#$(call package,freebsd,amd64,$(VERSION))
	#$(call package,openbsd,amd64,$(VERSION))
	#$(call package,netbsd,amd64,$(VERSION))

