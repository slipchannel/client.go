package main

import (
  "fmt"
  "io"
  "net/http"
  "os"
)

type KeyServer struct {
  Config Config
}

func (k KeyServer) GetPublicKey(username string) error {
  url := fmt.Sprintf("https://%s/%s/%s/%s/authorized_keys", k.Config.Service.Hostname, k.Config.Account.ExternalID, k.Config.Account.Category, username)

  response, err := http.Get(url)

  if err != nil {
    return err
  } else {
    defer response.Body.Close()

    if response.StatusCode == 200 {
      _, err := io.Copy(os.Stdout, response.Body)

      if err != nil {
        return err
      }
    }
  }

  return nil
}
