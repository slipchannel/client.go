package main

import (
  "errors"
  "fmt"
  "os"
  "io/ioutil"
  "path"

  "github.com/BurntSushi/toml"
)

var DEFAULT_CONFIG_CONTENT = `
[service]

# The hostname for the service.
# You should never need to change this.
hostname = "keys.slipchannel.com"

[account]

# Your external ID provided by Slipchannel
# URL: <url to find external ID>
external_id = ""

# The name of the category for this machine
# URL: <url to category list>
category = ""

`

type Config struct {
  Service struct {
    Hostname string `toml:"hostname"`
  }
  Account struct {
    ExternalID string  `toml:"external_id"`
    Category string `toml:"category"`
  }
}

func (c *Config) Load(path string) error {
  config_bytes, err := ioutil.ReadFile(path)
  if err != nil {
    return err
  }

  _, err = toml.Decode(string(config_bytes[:]), &c)
  if err != nil {
    return err
  }

  return nil
}

func generate_config_file(path string) error {
  fmt.Printf("Creating config file: %s\n", path)

  data := []byte(DEFAULT_CONFIG_CONTENT)

  err := ioutil.WriteFile(path, data, 0644)

  if err != nil {
    fmt.Fprintf(os.Stderr, "Cannot write to the file: %s", path)
    return err
  }

  return nil
}

// Checks whether the configuration file location is being overridden
// by a pre-defined environment variable and, if it does, returns true
// with the value of that environment variable; otherwise, false and
// an empty string.
func config_override_exists() (bool, string) {
  if env_var := os.Getenv(ENV_CONFIG_FILE); env_var != "" {
    return true, env_var
  }

  return false, ""
}

func first_time_setup(config_file string) error {
  fmt.Println("***** FIRST RUN DETECTED *****")
  fmt.Println("Cannot find configuration file.")
  fmt.Println("This may be the first time you have run this command.")
  fmt.Println("Creating config file:", config_file, ". You will need to move this into", DEFAULT_CONFIG_DIR, "manually")

  err := os.Mkdir(DEFAULT_CONFIG_DIR, 0755)

  if err != nil {
    fmt.Fprintf(os.Stderr, "Permission error writing to %s\n", DEFAULT_CONFIG_DIR)

    home_dir := os.Getenv("HOME")
    config_file = path.Join(home_dir, CONFIG_FILENAME)
    fmt.Fprintf(os.Stderr, "Falling back to use %s.  You will need to move this into %s manually.\n", home_dir, DEFAULT_CONFIG_DIR)

    // verify that there isn't already a file in there
    if file_exists(config_file) {
      msg := fmt.Sprintf("Error: It looks like you already have a config file at %s.  We don't want to clobber it, so how about you move it out of the way first and then retry this command?\n", config_file)
      return errors.New(msg)
    } else {
      generate_config_file(config_file)
    }
  } else {
    generate_config_file(config_file)
  }

  fmt.Println("Now populate the config file with the correct values")

  return nil
}

