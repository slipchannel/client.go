package main

import (
  "fmt"
  "os"
  "path"
  "path/filepath"

  "github.com/urfave/cli"
)

func main() {
  app := cli.NewApp()
  app.Name = "slipchannel"
  app.Usage = "SSH public key management, made simple."
  app.UsageText = "slipchannel [global options] USERNAME"
  app.Authors = []cli.Author{
    cli.Author{
      Name: "Slipchannel Development",
      Email: "support@slipchannel.com",
    },
  }
  app.Copyright = "(c) 2016 JustPlainSimple Technologies Inc."
  app.Version = Version.String()
  app.Commands = []cli.Command{}
  app.HideHelp = true
  app.HideVersion = true

  var showHelp, showVersion, doInit, doValidate bool
  config := Config{}

  app.Flags = []cli.Flag{
    cli.BoolFlag{
      Name: "init",
      Usage: "Initialize the client configuration file and exit",
      Destination: &doInit,
    },
    cli.BoolFlag{
      Name: "check",
      Usage: "Validate the client configuration file and exit",
      Destination: &doValidate,
    },
    cli.BoolFlag{
      Name: "help, h",
      Usage: "Display this message and exit",
      Destination: &showHelp,
    },
    cli.BoolFlag{
      Name: "version, v",
      Usage: "Print the version and exit",
      Destination: &showVersion,
    },
  }

  app.Action = func(c *cli.Context) error {
    config_file := path.Join(DEFAULT_CONFIG_DIR, CONFIG_FILENAME)

    if c.NArg() > 0 {
      username := c.Args().Get(0)

      err := config.Load(config_file)

      if err != nil {
        fmt.Fprintf(os.Stderr, "Error loading config: %s.  Error message: %s\n", config_file, err.Error())
        os.Exit(1)
      }

      keyserver := KeyServer{Config: config}
      err = keyserver.GetPublicKey(username)
      if err != nil {
        fmt.Fprintf(os.Stderr, "%s\n", err.Error())
        os.Exit(1)
      }
    } else {
      if showHelp {
        cli.ShowAppHelp(c)
        return nil
      }

      if showVersion {
        cli.ShowVersion(c)
        return nil
      }

      if doInit {
        if exists, env_var := config_override_exists(); exists {
          if filepath.IsAbs(env_var) {
            config_file = env_var
          } else {
            fmt.Fprintf(os.Stderr, "You specified an override that is using a relative path (%s).  This will not work as you don't always know what directory the OpenSSH daemon will be calling the client. Convert the path to absolute and re-run this command.\n", env_var)
            os.Exit(1)
          }
        }

        if file_missing(config_file) {
          err := first_time_setup(config_file)

          if err != nil {
            fmt.Fprintf(os.Stderr, "%s\n", err.Error())
          }
          os.Exit(1)
        }

        return nil
      }

      if doValidate {
        err := config.Load(config_file)

        if err != nil {
          fmt.Fprintf(os.Stderr, "Error loading config: %s.  Error message: %s\n", config_file, err.Error())
          os.Exit(1)
        }

        return nil
      }

      cli.ShowAppHelp(c)
    }
    return nil
  }

  app.Run(os.Args)
}

