package main

// Adapted from:
// https://ariejan.net/2015/10/12/building-golang-cli-tools-update/

import "fmt"

type version struct {
  Major, Minor, Patch int
}

var Version = version{1, 0, 0}

var Build string

func (v version) String() string {
  return fmt.Sprintf("%d.%d.%db%s", v.Major, v.Minor, v.Patch, Build)
}

