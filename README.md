# Slipchannel Client

The Slipchannel client written in [Go](https://golang.org/) for simple
deployment to other systems.

This client connects to Slipchannel and returns the SSH public keys
for the given username.  Due to limitations of how this client is
called by OpenSSH, only the username is permitted on the command
line.  The configuration file resides in a specific place
`/etc/slipchannel/config.toml`.

## Usage

```
$ slipchannel
NAME:
   slipchannel - SSH public key management, made simple.

USAGE:
   slipchannel [global options] USERNAME

AUTHOR(S):
   Slipchannel Development <support@slipchannel.com>

GLOBAL OPTIONS:
   --init         Initialize the client configuration file and exit
   --check        Validate the client configuration file and exit
   --help, -h     Display this message and exit
   --version, -v  Print the version and exit

COPYRIGHT:
   (c) 2016 JustPlainSimple Technologies Inc.
```

## Getting Started

When `slipchannel` is first run, it notices that a configuration file
does not exist in the default place.  It will attempt to write a new
`config.toml` file to this place and, if it does not have the correct
permissions, it will write the file to the user's home directory.

## Downloading a Release

Check the Releases page for the latest version.

## Downloading the Source Code

You can do this in your terminal:

`go get gitlab.com/slipchannel/client.go`

