package main

import (
  "os"
)

func file_exists(path string) bool {
  if _, err := os.Stat(path); os.IsNotExist(err) {
    return false
  }

  return true
}

func file_missing(path string) bool {
  return !file_exists(path)
}

