package main

// Absolute path to where slipchannel client configuration files are stored
const DEFAULT_CONFIG_DIR = "/etc/slipchannel"

// The name of the client configuration file
const CONFIG_FILENAME = "client.toml"

// The name of the environment variable that is used to override the
// default location (see above) of the client configuration file.
const ENV_CONFIG_FILE = "SLIPCHANNEL_CONFIG_FILE"

