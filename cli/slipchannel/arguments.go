package main

import (
  "errors"
  "fmt"
  "os"
)

type Arguments struct {
  ProgramName string
  Username string
}

func (a *Arguments) parse_args() error {
  args := os.Args[1:]
  a.ProgramName = os.Args[0]

  if len(args) < 1 {
    msg := fmt.Sprintf("Usage: %s USERNAME\n", a.ProgramName)
    return errors.New(msg)
  }
  a.Username = args[0]
  return nil
}

